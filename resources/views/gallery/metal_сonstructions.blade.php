@extends('layouts.app')

@section('title','Мет.конструкции')

 <!--                      -->
   <!--          Металлические конструкции    -->
 <!--                                               -->
@section('content')
<div class="container-fluid gallery_content" id="metal_сonstructions">
  <section class="section">
    <div class="gallery_intro_images" id="metal_сonstructions_intro_img">
      <div class="gallery_section_content">
        <h1>Металлические конструкции</h1>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
<!-- Место для фото -->
  </section>
  <div class="gallery_ending_images" id="metal_сonstructions_ending_img"> </div>
</div>
@endsection
