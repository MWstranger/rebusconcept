@extends('layouts.app')

@section('title','Столы')

 <!--                      -->
   <!--                   Столы    -->
 <!--                                               -->
@section('content')
<div class="container-fluid gallery_content" id="tables">
  <section class="section">
    <div class="gallery_intro_images" id="tables_intro_img">
      <div class="gallery_section_content">
        <h1>Столы</h1>
        <a class="subsection_link" href="#table_transformer">Cтол трансформер</a>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/9.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/9.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/10.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/10.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/11.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/11.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section gallery_content_subparagraph" id="table_transformer">
    <div class="gallery_sub_images" id="table_transformer_sub_img1">
      <div class="gallery_section_content">
        <a class="subsection_link" href="#tables">Столы</a>
        <h2>Стол трансформер</h2>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/table_transformer/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/table_transformer/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/table_transformer/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/table_transformer/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/tables/table_transformer/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/tables/table_transformer/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="gallery_ending_images" id="tables_ending_img"> </div>
</div>
@endsection
