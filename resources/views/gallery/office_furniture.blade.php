@extends('layouts.app')

@section('title','Офисная мебель')

 <!--                      -->
   <!--             Офисная мебель    -->
 <!--                                               -->
@section('content')
<div class="container-fluid gallery_content" id="office_furniture">
  <section class="section">
    <div class="gallery_intro_images" id="office_furniture_intro_img">
      <div class="gallery_section_content">
        <h1>Офисная мебель</h1>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/9.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/9.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/10.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/10.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/11.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/11.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/12.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/12.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/13.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/13.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/14.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/14.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/officeFurniture/15.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/officeFurniture/15.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="gallery_ending_images" id="office_furniture_ending_img"> </div>
</div>
@endsection
