@extends('layouts.app')

@section('title','Массив дерева')

 <!--                      -->
   <!--                   Массив дерева    -->
 <!--                                               -->
@section('content')
<div class="container-fluid gallery_content" id="solid_wood">
  <section class="section">
    <div class="gallery_intro_images" id="solid_wood_intro_img">
      <div class="gallery_section_content">
        <h1>Массив дерева</h1>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/9.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/9.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/10.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/10.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/11.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/11.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/12.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/12.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/13.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/13.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/14.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/14.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/15.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/15.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/16.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/16.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/17.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/17.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/18.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/18.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/solidWood/19.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/solidWood/19.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="gallery_ending_images" id="solid_wood_ending_img"> </div>
</div>
@endsection
