@extends('layouts.app')

@section('title','Кухни')

 <!--                      -->
   <!--                   Кухни    -->
 <!--                                               -->
@section('content')
<div class="container-fluid gallery_content" id="modern_kitchens">
  <section class="section gallery_content">
    <div class="gallery_intro_images" id="kitchens_intro_img">
      <div class="gallery_section_content">
        <h1>Современные кухни</h1>
        <a class="subsection_link" href="#classic_kitchens">Классические кухни</a>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/9.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/9.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/10.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/10.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/11.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/11.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/modernKitchens/12.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/modernKitchens/12.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section gallery_content_subparagraph" id="classic_kitchens">
    <div class="gallery_sub_images" id="kitchens_sub_img1">
      <div class="gallery_section_content">
        <a class="subsection_link" href="#modern_kitchens">Современные кухни</a>
        <h2>Классические кухни</h2>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/9.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/9.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/10.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/10.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/11.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/11.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/12.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/12.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/kitchens/classicKitchens/13.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/kitchens/classicKitchens/13.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="gallery_ending_images" id="kitchens_ending_img"> </div>
</div>
@endsection
