@extends('layouts.app')

@section('title','Детские')

 <!--                      -->
   <!--                   Детские    -->
 <!--                                               -->
@section('content')
<div class="container-fluid gallery_content" id="childrens_rooms">
  <section class="section">
    <div class="gallery_intro_images" id="childrens_rooms_intro_img">
      <div class="gallery_section_content">
        <h1>Детские</h1>
        <a class="subsection_link" href="#beds">Кровати</a>
        <a class="subsection_link" href="#nursery_school">Детский сад</a>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/9.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/9.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section gallery_content_subparagraph" id="beds">
    <div class="gallery_sub_images" id="childrens_rooms_sub_img1">
      <div class="gallery_section_content">
        <a class="subsection_link" href="#childrens_rooms">Детские</a>
        <a class="subsection_link" href="#nursery_school">Детский сад</a>
        <h2>Кровати</h2>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/bads/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/bads/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/bads/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/bads/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/bads/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/bads/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/bads/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/bads/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/bads/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/bads/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/bads/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/bads/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/bads/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/bads/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/bads/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/bads/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/bads/9.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/bads/9.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/bads/10.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/bads/10.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/bads/11.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/bads/11.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section gallery_content_subparagraph" id="nursery_school">
    <div class="gallery_sub_images" id="childrens_rooms_sub_img2">
      <div class="gallery_section_content">
        <a class="subsection_link" href="#childrens_rooms">Детские</a>
        <a class="subsection_link" href="#beds">Кровати</a>
        <h2>Детский сад</h2>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/nursery_school/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/nursery_school/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/nursery_school/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/nursery_school/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/nursery_school/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/nursery_school/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/nursery_school/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/nursery_school/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/nursery_school/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/nursery_school/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/nursery_school/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/nursery_school/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/nursery_school/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/nursery_school/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/nursery_school/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/nursery_school/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/childrensRooms/nursery_school/9.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/childrensRooms/nursery_school/9.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="gallery_ending_images" id="childrens_rooms_ending_img"> </div>
</div>
@endsection
