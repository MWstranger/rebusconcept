@extends('layouts.app')

@section('title','Двери')

 <!--                      -->
   <!--                   Двери    -->
 <!--                                               -->
@section('content')
<div class="container-fluid gallery_content" id="doors">
  <section class="section">
    <div class="gallery_intro_images" id="doors_intro_img">
      <div class="gallery_section_content">
        <h1>Двери</h1>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/9.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/9.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/10.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/10.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/11.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/11.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/12.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/12.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/doors/13.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/doors/13.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="gallery_ending_images" id="doors_ending_img"> </div>
</div>
@endsection
