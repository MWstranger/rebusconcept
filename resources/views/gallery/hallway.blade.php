@extends('layouts.app')

@section('title','Прихожие')

 <!--                      -->
   <!--                   Прихожие    -->
 <!--                                               -->
@section('content')
<div class="container-fluid gallery_content" id="hallway">
  <section class="section">
    <div class="gallery_intro_images" id="hallway_intro_img">
      <div class="gallery_section_content">
        <h1>Прихожие</h1>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/hallway/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/hallway/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/hallway/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/hallway/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/hallway/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/hallway/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/hallway/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/hallway/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/hallway/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/hallway/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/hallway/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/hallway/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/hallway/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/hallway/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/hallway/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/hallway/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/hallway/9.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/hallway/9.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="gallery_ending_images" id="hallway_ending_img"> </div>
</div>
@endsection
