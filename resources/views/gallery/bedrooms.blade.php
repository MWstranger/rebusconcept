@extends('layouts.app')

@section('title','Спальни')

 <!--                      -->
   <!--                   Спальни    -->
 <!--                                               -->
@section('content')
<div class="container-fluid gallery_content" id="bedrooms">
  <section class="section">
    <div class="gallery_intro_images" id="bedrooms_intro_img">
      <div class="gallery_section_content">
        <h1>Спальни</h1>
        <a class="subsection_link" href="#beds">Кровати</a>
        <a class="subsection_link" href="#console_mirror">Трюмо</a>
        <a class="subsection_link" href="#cupboard">Шкафы</a>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section gallery_content_subparagraph" id="beds">
    <div class="gallery_sub_images" id="bedrooms_sub_img1">
      <div class="gallery_section_content">
        <a class="subsection_link" href="#bedrooms">Cпальни</a>
        <a class="subsection_link" href="#console_mirror">Трюмо</a>
        <a class="subsection_link" href="#cupboard">Шкафы</a>
        <h2>Кровати</h2>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/beds/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/beds/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/beds/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/beds/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/beds/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/beds/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/beds/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/beds/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/beds/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/beds/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/beds/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/beds/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/beds/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/beds/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/beds/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/beds/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section gallery_content_subparagraph" id="console_mirror">
    <div class="gallery_sub_images" id="console_mirror_img">
      <div class="gallery_section_content">
        <a class="subsection_link" href="#bedrooms">Cпальни</a>
        <a class="subsection_link" href="#beds">Кровати</a>
        <a class="subsection_link" href="#cupboard">Шкафы</a>
        <h2>Трюмо</h2>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/console_mirror/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/console_mirror/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/console_mirror/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/console_mirror/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/console_mirror/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/console_mirror/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/console_mirror/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/console_mirror/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/console_mirror/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/console_mirror/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/console_mirror/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/console_mirror/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/console_mirror/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/console_mirror/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section gallery_content_subparagraph" id="cupboard">
    <div class="gallery_sub_images" id="cupboard_img">
      <div class="gallery_section_content">
        <a class="subsection_link" href="#bedrooms">Cпальни</a>
        <a class="subsection_link" href="#beds">Кровати</a>
        <a class="subsection_link" href="#console_mirror">Трюмо</a>
        <h2>Шкафы</h2>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/cupboard/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/cupboard/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/bedrooms/cupboard/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/bedrooms/cupboard/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="gallery_ending_images" id="bedrooms_ending_img"> </div>
</div>
@endsection
