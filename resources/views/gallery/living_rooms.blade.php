@extends('layouts.app')

@section('title','Гостинные')

 <!--                      -->
   <!--                   Гостинные    -->
 <!--                                               -->
@section('content')
<div class="container-fluid gallery_content" id="living_rooms">
  <section class="section">
    <div class="gallery_intro_images" id="living_rooms_intro_img">
      <div class="gallery_section_content">
        <h1>Гостинные</h1>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/livingRooms/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/livingRooms/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/livingRooms/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/livingRooms/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/livingRooms/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/livingRooms/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/livingRooms/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/livingRooms/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="gallery_ending_images" id="living_rooms_ending_img"> </div>
</div>
@endsection
