@extends('layouts.app')

@section('title','Шкафы купе')

 <!--                      -->
   <!--                   Шкафы купе    -->
 <!--                                               -->
@section('content')
<div class="container-fluid gallery_content" id="wardrobes">
  <section class="section">
    <div class="gallery_intro_images" id="wardrobes_intro_img">
      <div class="gallery_section_content">
        <h1>Шкафы купе</h1>
      </div>
    </div>
  </section>
  <section class="section gallery_images container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/1.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/1.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/2.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/2.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/3.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/3.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/4.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/4.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/5.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/5.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/6.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/6.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/7.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/7.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/8.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/8.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/9.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/9.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/10.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/10.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/11.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/11.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/12.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/12.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/13.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/13.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/14.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/14.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/15.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/15.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/16.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/16.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/17.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/17.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/18.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/18.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/19.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/19.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/20.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/20.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/21.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/21.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/22.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/22.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/23.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/23.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/24.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/24.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/25.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/25.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/26.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/26.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/27.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/27.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/28.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/28.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/29.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/29.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/30.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/30.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/31.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/31.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/32.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/32.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/33.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/33.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/34.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/34.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <div class="card-content">
            <a href="../img/gallery/wardrobes/35.jpg" data-rel="lightcase:myCollection">
              <img class="" src="../img/gallery/wardrobes/35.jpg" alt="REBUS concept - фото из галереи">
            </a>
          </div>
        </div>
      </div>
  </section>
  <div class="gallery_ending_images" id="wardrobes_ending_img"> </div>
</div>
@endsection
