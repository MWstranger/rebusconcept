<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <!--   Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--          Title -->
  <title>@yield('title')</title>
  <!--                            Styles -->
  <!--                                          Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Atomic+Age|Audiowide|Bad+Script|Cormorant+Garamond:300,300i,400,400i,500,500i,600,600i,700,700i|Cormorant+Unicase:300,400,500,600,700|Didact+Gothic|Kelly+Slab|Oswald:200,300,400,500,600,700|Playfair+Display:400,400i,700,700i,900,900i|Righteous|Stalinist+One&amp;subset=cyrillic" rel="stylesheet"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('css/main.css') }}">
  <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
  <!-- CSS для модальных окон lightcase -->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/lightcase.css') }}">
  <script scr="{{ asset('js/prefixfree.min.js') }}"></script>
</head>
<body>
 <div id="app">


<!--            -->
   <!--          Дополнительная панель -->
 <!--                                   -->
<!--    <div id="add_panel">
     <ul class="add_bar">
       <li class="navbar-brand">
         <h1>
           <a href="/">
            REBUS concept 
            <i class="fa fa-connectdevelop" aria-hidden="true"></i>
           </a>
         </h1>
       </li>
       <li>
         <a href="/future_plans">
          <i class="fa fa-fax" aria-hidden="true"></i>
         Дальнейшие планы
         </a>
       </li>
       <li>
         <a href="#footer">
          <i class="fa fa-fax" aria-hidden="true"></i>
         Контакты
         </a>
       </li>
     </ul>
   </div> -->
<!--       -->
<!--            -->


<!--            -->
   <!--          панель меню -->
 <!--                                   -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top">
   <!-- логотип -->
    <span class="navbar-brand mb-0 h1">
     <a href="/">
      REBUS concept 
      <i class="fa fa-connectdevelop" aria-hidden="true"></i>
     </a> 
    </span>
   <!-- кнопка сворачивания меню -->
    <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#REBUS_menu" aria-controls="REBUS_menu" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
   <!-- списки и пункты меню -->
    <div class="collapse navbar-collapse" id="REBUS_menu">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
           Главная
           <span class="sr-only">(current)</span> 
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
            <a class="dropdown-item" href="/">Главная</a>
<!--             <a class="dropdown-item" href="/#feedbacks">Отзывы клиентов</a> -->
            <a class="dropdown-item" href="/#order">Как проходит заказ</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/#аpplication">Оставить заявку</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/#about_us">О нас</a>
          </div>
        </li>
        <li class="nav-item">
         <a class="nav-link" href="/doors">
          <i class="fa fa-cubes" aria-hidden="true"></i>
         Двери
         </a>
        </li>
        <li class="nav-item">
         <a class="nav-link" href="/solid_wood">
          <i class="fa fa-cubes" aria-hidden="true"></i>
         Массив дерева
         </a>
        </li>
        <li class="nav-item">
         <a class="nav-link" href="/materials">
          <i class="fa fa-cubes" aria-hidden="true"></i>
         Материалы
         </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           <i class="fa fa-camera-retro" aria-hidden="true"></i>
          Галерея
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
            <a class="dropdown-item" href="/gallery/living_rooms">Гостинные</a>
            <a class="dropdown-item" href="/gallery/doors">Двери</a>
            <a class="dropdown-item" href="/gallery/childrens_rooms">Детские</a>
            <a class="dropdown-item" href="/gallery/kitchens">Кухни</a>
            <a class="dropdown-item" href="/gallery/solid_wood">Массив дерева</a>
<!--             <a class="dropdown-item" href="/gallery/metalConstructions">Металлоконструкции</a> -->
            <a class="dropdown-item" href="/gallery/office_furniture">Офисная мебель</a>
            <a class="dropdown-item" href="/gallery/hallway">Прихожие</a>
            <a class="dropdown-item" href="/gallery/bedrooms">Спальни</a>
            <a class="dropdown-item" href="/gallery/tables">Столы</a>
            <a class="dropdown-item" href="/gallery/wardrobes">Шкафы купе</a>
          </div>
        </li>
        <li class="nav-item" id="nav_item_contacts">
         <a class="nav-link" href="#footer">
          <i class="fa fa-fax" aria-hidden="true"></i>
         Контакты
         </a>
        </li>
      </ul>
    </div>
  </nav>
<!-- -->


<!--        -->
    <!-- Навигаторы (стрелка "наверх") -->
  <!--                  -->
  <div id="upbutton" href="#" onclick="smoothJumpUp(); return false;">
      <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
  </div>
<!-- -->


<!-- контент страниц -->
  @yield('content')
<!--        -->


<!--   футер    -->
  <footer>
      <div class="flex_container" id="footer">
        <div class="flexbox">
          <h5>Наш адрес:</h5>
          <p>Украина. Николаев.<br>
           ул. 12-я Продольная 63а.</p>
        </div>
        <div class="flexbox">
          <h5>Наш телефон:</h5>
          <p>+380667855305</p>
          <h5>Наш e-mail:</h5>
          <p>rebusconcept@gmail.com</p>
        </div>
        <div class="flexbox">
          <h5>График работы:</h5>
          <p>
           Понедельник-Пятница<br>
           &nbsp;с 10:00 до 18:00<br>
          </p>
        </div>
        <div class="flexbox" id="flexbox4">
          <h5>Мы в социальных сетях:</h5>
          <a href="https://www.facebook.com/"><img src="../img/icon/facebook_icon.jpeg" alt="facebook"></a>
          <a href="https://www.instagram.com/"><img src="../img/icon/Instagram_icon.jpeg" alt="instagram"></a>
        </div>
      </div>
  </footer>
 </div>

  <!--       ===   =====        scripts     =====   ===         -->
    <!-- jQuery первым, затем Popper.js, замет Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!--   JS-файл от фрэймфорка    -->
    <script src="{{ asset('js/app.js') }}"></script> 
    <!--        Свой   JS-файл проэкта -->
    <script src="{{ asset('js/REBUS.js') }}"></script> 
    <!-- JS и jQuery для модальных окон lightcase -->
    <script type="text/javascript" src="{{ asset('js/lightcase.js') }}"></script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $('a[data-rel^=lightcase]').lightcase();
      });
    </script>
</body>
</html>
