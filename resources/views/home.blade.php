@extends('layouts.app')

@section('title','Главная')

@section('content')
<div id="landing">

 <!--       -->
   <!--        Интро   -->
 <!--                                               -->
  <section id="intro">
    <div id="intro_content_background"></div>
    <div id="intro_content">
      <h1>
       REBUS concept
       <i class="fa fa-connectdevelop" aria-hidden="true"></i>
      </h1>
      <p>
       Создаем целое из фрагментов вместе...
      </p>
    </div>
  </section>
<!--                             -->
        <!--     Cлайдер              -->
 <!--                                               -->
  <section id="REBUS_carousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active">
        <img src="../img/carousel/1.jpeg" class="d-block" alt="1 slide">
        <div class="carousel-caption">
<!--           <h3>Название товара</h3>
          <p>Комментарии к товару...</p> -->
        </div>
      </div>
      <div class="carousel-item">
        <img src="../img/carousel/2.jpg" class="d-block" alt="2 slide">
        <div class="carousel-caption">
        </div>
      </div>
      <div class="carousel-item">
        <img src="../img/carousel/3.jpg" class="d-block" alt="3 slide">
        <div class="carousel-caption">
        </div>
      </div>
      <div class="carousel-item">
        <img src="../img/carousel/4.jpg" class="d-block" alt="4 slide">
        <div class="carousel-caption">
        </div>
      </div>
      <div class="carousel-item">
        <img src="../img/carousel/5.jpeg" class="d-block" alt="5 slide">
        <div class="carousel-caption">
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#REBUS_carousel" role="button" data-slide="prev">

      <span aria-hidden="true">
        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
      </span>
      <span class="sr-only">Предыдущий</span>
    </a>
    <a class="carousel-control-next" href="#REBUS_carousel" role="button" data-slide="next">
      <span aria-hidden="true">
        <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
      </span>
      <span class="sr-only">Следующий</span>
    </a>
  </section>

<!--                      -->
   <!--         Качества компании      -->
 <!--                                               -->
  <div id="merit"> 
    <h2>Почему выбирают нас:</h2>
    <div class="row">
      <div class="col-sm-6">
        <i class="fa fa-hourglass-half animated scroll-flip" aria-hidden="true"></i>
        <p class="">
         Более 20 лет работы в мебельной сфере.
        </p>
      </div>
      <div class="col-sm-6">
        <i class="fa fa-calendar animated scroll-flip" aria-hidden="true"></i>
        <p class="">
         18 месяцев гарантии на наши изделия. *
        </p>
      </div>
      <div class="col-sm-6">
        <i class="fa fa-credit-card animated scroll-flip" aria-hidden="true"></i>
        <p class="">
         Удобные формы оплаты: наличные, банковская карта.
        </p>
      </div>
      <div class="col-sm-6">
        <i class="fa fa-map-o animated scroll-flip" aria-hidden="true"></i>
        <p class="">
         Сотни фасадов различных расцветок и материалов.
        </p>
      </div> 
      <div class="col-sm-6">
        <i class="fa fa-compress animated scroll-flip" aria-hidden="true"></i>
        <p class="">
         Cобственное производство: мебель корректно впишется в ваше пространство.
        </p>
      </div>
      <div class="col-sm-6">
        <i class="fa fa-wrench animated scroll-flip" aria-hidden="true"></i>
        <p class="">
         Аккуратно соберем и установим мебель.
        </p>
      </div>
    </div> <!--  end row -->
    <div id="guarantees">
      <i class="fa fa-eercast animated scroll-flip" aria-hidden="true"></i>
      <p>
       *Мы даём гарантию на все наши изделия, кроме фурнитуры.<br>
       **За исключением случаев неосторожной эксплуатации или преднамеренного повреждения по вине покупателя.<br>
       ***В случае выявления покупателем каких-либо дефектов, мы обязуемся устранить изъян в течение 20 дней.<br>
      </p>
    </div>
  </div>

 <!--                       -->
   <!--                 Отзывы клиентов    -->
 <!--                                               -->
<!--   <div id="feedbacks">
    <div id="feedbacks_content">
      <h2>Отзывы наших клиентов:</h2>
      <h5>
        Более 80% клиентов обращаются к нам по рекомендации друзей, которые заказывали у нас мебель.<br>Это лучшее доказательство качества нашей работы.
      </h5>
    </div>
    <div id="feedbacks_second_content">
      <div class="row">
          <div class="col-md-4">
            <img class="img_circle" src="../img/clients_photo/j.jpeg" alt="costumer photo" width="200" height="200">
            <h3>First Second names</h3>
            <p>
             Первый отзыв о REBUSе.
            </p>
          </div>
          <div class="col-md-4">
            <img class="img_circle" src="../img/clients_photo/w.jpeg" alt="costumer photo" width="200" height="200">
            <h3>First Second names</h3>
            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
          </div>
          <div class="col-md-4">
            <img class="img_circle" src="../img/clients_photo/y.jpeg" alt="costumer photo" width="200" height="200">
            <h3>First Second names</h3>
            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-4">
            <img class="img_circle" src="../img/clients_photo/k.jpeg" alt="costumer photo" width="200" height="200">
            <h3>First Second names</h3>
            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
          </div>
          <div class="col-md-4">
            <img class="img_circle" src="../img/clients_photo/r.jpeg" alt="costumer photo" width="200" height="200">
            <h3>First Second names</h3>
            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
          </div>
          <div class="col-md-4">
            <img class="img_circle" src="../img/clients_photo/x.jpeg" alt="costumer photo" width="200" height="200">
            <h3>First Second names</h3>
            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
          </div>
      </div>
    </div>
  </div>
 -->
 <!--                       -->
   <!--                Как проходит заказ                 -->
 <!--                                               -->
  <div id="order">
    <h2>Как проходит заказ:</h2>
    <ul>
      <li class="animated scroll-bounce">
       Визит замерщика в удобное для вас время
       <img class="order_icons" src="../img/order/1.png" alt="order_icons">
       <div class="order_phone_icons">
         <img src="../img/order/1.png" alt="order_icons">
       </div>
      </li>
      <li class="animated scroll-bounce">
       Разработка дизайн-проекта
       <img class="order_icons" src="../img/order/2.png" alt="order_icons">
       <div class="order_phone_icons">
         <img src="../img/order/2.png" alt="order_icons">
       </div>
      </li>
      <li class="animated scroll-bounce">
       Ваш визит к нам в офис
       <img class="order_icons" src="../img/order/3.png" alt="order_icons">
       <div class="order_phone_icons">
         <img src="../img/order/3.png" alt="order_icons">
       </div>
      </li>
      <li class="animated scroll-bounce">
       Производство по индивидуальным размерам
       <img class="order_icons" src="../img/order/4.png" alt="order_icons">
       <div class="order_phone_icons">
         <img src="../img/order/4.png" alt="order_icons">
       </div>
      </li>
      <li class="animated scroll-bounce">
       Бережная доставка
       <img class="order_icons" src="../img/order/5.png" alt="order_icons">
       <div class="order_phone_icons">
         <img src="../img/order/5.png" alt="order_icons">
       </div>
      </li>
      <li class="animated scroll-bounce">
       Установка у вас дома
       <img class="order_icons" src="../img/order/6.png" alt="order_icons">
       <div class="order_phone_icons">
         <img src="../img/order/6.png" alt="order_icons">
       </div>
      </li>
    </ul>
  </div>

 <!--                       -->
   <!--             Бланк      -->
 <!--                                               -->
  <div id="blank">
    <h2>Ваша новая мебель ждет вас!</h2>
    <h4>Оставить заявку...</h4>
    <a href="/#аpplication"><i class="fa fa-arrow-circle-down" aria-hidden="true"></i></a>
  </div>

 <!--                             -->
   <!--         Заявка    -->
 <!--                                               -->
  <div id="request">
    <div id="request_intro">
      <div id="аpplication"> </div> <!-- якорь на заявку -->
      <div id="аpplication_form"> <!-- контейнер формы -->
        <form>

          <!-- Hidden Required Fields -->
          <input type="hidden" name="project_name" value="REBUSconcept">
          <input type="hidden" name="admin_email" value=" rebus@rebusconcept.com.ua">
          <input type="hidden" name="form_subject" value="Заявка с REBUSconcept">
          <!-- END Hidden Required Fields -->

          <div id="app_conteiner">
            <div class="form-group">
             <label for="name">Имя:</label>
             <input type="text" name="Имя" class="form-control form-control-lg" id="name" placeholder="Ваше имя...">
            </div>
            <div class="form-group">
              <label for="E-mail">Адрес почты:</label>
              <input type="text" name="E-mail" class="form-control form-control-lg" id="E-mail" aria-describedby="emailHelp" placeholder="Ваш E-mail...">
            </div>
            <div class="form-group">
              <label for="phone">Номер телефона:</label>
              <input type="text" name="Телефон" class="form-control form-control-lg" id="phone" placeholder="Ваш номер телефона...">
              <small id="phone" class="form-text text-muted">*вы можете ввести несколько номеров, через запятую.</small>
            </div>
            <div class="form-group">
              <label for="textArea">Поле для текста:</label>
              <input  type="text" name="Текст"  class="form-control form-control-lg" id="textArea" placeholder="Введите дополнительные комментарии..."></textarea>
            </div>
          </div>

          <button type="submit" class="btn btn-outline-success btn-lg btn-block">Отправить заявку</button>
          <br>

          <p>
           &nbsp;<b>ВНИМАНИЕ!</b> Иногда заявка может не дойти до нас
           (такое происходит крайне редко, но все же).<br>
           Если мы не ответили вам в течении нескольких дней, пожалуйста, позвоните нам или напишите на наш e-mail.
          </p>
        </form> 
      </div> <!-- конец контейнера формы -->
    </div>
  </div>

  <!--                             -->
   <!--        О нас                -->
 <!--                       -->
  <div id="about_us">
    <h2>О нас</h2>
    <p>
     &nbsp;&nbsp;Мы начинали свою деятельность в 2003 году с небольшой мастерской по изготовлению корпусной мебели на заказ. Сейчас мы развиваемся по многим направлениям, которые включают в себя индивидуальный подход к каждому, кто сотрудничает с нами.<br>
     &nbsp;&nbsp;ВЕРА в будущее, ЖЕЛАНИЕ развиваться, постоянно нарабатываемый ОПЫТ, РАБОТА над ошибками, все это помогает нам в ДВИЖЕНИИ ВПЕРЕД. Мы стараемся МАКСИМАЛЬНО услышать все ваши пожелания и ответственно подходим к поставленной задаче. Если мы чего-то не знаем - то попытаемся узнать, если не готовы выполнить - посоветуем того, кто в этом мастер.<br><br>
     &nbsp;&nbsp;Ждём новых заявок, чтобы воплотить ваши идеи в реальность.<br>
    </p>
    <div id="about_us_img"></div>
  </div>

  <!--                             -->
   <!--        Гугл-карта                    -->
 <!--                       -->
  <div id="google_map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1361.8903465460673!2d32.06051967602184!3d46.94634922586002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c5cb8fb8ac867f%3A0x85e958da18f8096c!2sREBUSconcept!5e0!3m2!1sru!2sua!4v1528202206282" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>

<!-- Cкрипты для анимации -->
<script src="{{ asset('js/particles.js') }}"></script>
  <!-- 
    параметры скрипта
    первый параметр контейнер с id (в котором будет анимация)
    второй параметр json файл в котором будут лежать "конфигурации"
    третий параметр фукнция, которая будет возвращать нам ответ в браузере, что файл json успешно запущен
   -->
<script>
  particlesJS.load('intro','particles.json');
</script>

@endsection