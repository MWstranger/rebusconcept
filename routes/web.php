<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//main view
Route::get('/future_plans', 'future_plansController@future_plans');
Route::get('/', 'homeController@home');
Route::get('/doors', 'doorsController@doors');
Route::get('/solid_wood', 'solid_woodController@solid_wood');
Route::get('/materials', 'materialsController@materials');
Route::get('/gallery', 'galleryController@gallery');
//gallery view
Route::get('/gallery/bedrooms', 'gallery\bedroomsController@bedrooms');
Route::get('/gallery/childrens_rooms', 'gallery\childrens_roomsController@childrens_rooms');
Route::get('/gallery/doors', 'gallery\doorsController@doors');
Route::get('/gallery/hallway', 'gallery\hallwayController@hallway');
Route::get('/gallery/kitchens', 'gallery\kitchensController@kitchens');
Route::get('/gallery/living_rooms', 'gallery\living_roomsController@living_rooms');
Route::get('/gallery/metal_constructions', 'gallery\metal_constructionsController@metal_constructions');
Route::get('/gallery/office_furniture', 'gallery\office_furnitureController@office_furniture');
Route::get('/gallery/solid_wood', 'gallery\solid_woodController@solid_wood');
Route::get('/gallery/tables', 'gallery\tablesController@tables');
Route::get('/gallery/wardrobes', 'gallery\wardrobesController@wardrobes');





