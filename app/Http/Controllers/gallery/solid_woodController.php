<?php

namespace App\Http\Controllers\gallery;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class solid_woodController extends Controller
{
    public function solid_wood()
    {
        return view('gallery/solid_wood');
    }
}
