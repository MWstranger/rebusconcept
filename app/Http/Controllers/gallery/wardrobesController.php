<?php

namespace App\Http\Controllers\gallery;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class wardrobesController extends Controller
{
    public function wardrobes()
    {
        return view('gallery/wardrobes');
    }
}
