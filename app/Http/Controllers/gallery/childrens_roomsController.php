<?php

namespace App\Http\Controllers\gallery;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class childrens_roomsController extends Controller
{
    public function childrens_rooms()
    {
        return view('gallery/childrens_rooms');
    }
}
