<?php

namespace App\Http\Controllers\gallery;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class living_roomsController extends Controller
{
    public function living_rooms()
    {
        return view('gallery/living_rooms');
    }
}
