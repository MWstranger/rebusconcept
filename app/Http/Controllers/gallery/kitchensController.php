<?php

namespace App\Http\Controllers\gallery;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class kitchensController extends Controller
{
    public function kitchens()
    {
        return view('gallery/kitchens');
    }
}
