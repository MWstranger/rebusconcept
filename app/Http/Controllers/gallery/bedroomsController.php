<?php

namespace App\Http\Controllers\gallery;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class bedroomsController extends Controller
{
    public function bedrooms()
    {
        return view('gallery/bedrooms');
    }
}
