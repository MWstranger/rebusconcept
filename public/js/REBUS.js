//////////////////////////////////////////////
/////Скрипт отправки заявок на почту!
$(document).ready(function() {

    //E-mail Ajax Send
    $("form").submit(function() { //Change
        var th = $(this);
        $.ajax({
            type: "POST",
            url: "mail.php", //Change
            data: th.serialize()
        }).done(function() { 
            alert("Заявка успешно отправлена. Хорошего вам дня! *)");
            setTimeout(function() {
                // Done Functions
                th.trigger("reset");
            }, 1000);
        });
        return false;
    });

});



/////////////////////////////////////////////
/////cкрипт для кнопки-навигации
$(window).scroll(function() {
    if ($(this).scrollTop() > 1500) {
        if ($('#upbutton').is(':hidden')) {
            $('#upbutton').css({opacity : 1}).fadeIn('slow');
        }
    } else { $('#upbutton').stop(true, false).fadeOut('fast'); }
});
$('#upbutton').click(function() {
    $('html, body').stop().animate({scrollTop : 0}, 300);
});



/////////////////////////////////////////////
/////cкрипт для анимации текста на сайте
$(window).scroll(function() {
    $('.scroll-flip').each(function() {   //text-animation
        var textAnimation = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        if (textAnimation < topOfWindow+500) {
            $(this).addClass('flip');
        }
    });
});
$(window).scroll(function() {
    $('.scroll-bounce').each(function() {   //text-animation
        var textAnimation = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        if (textAnimation < topOfWindow+500) {
            $(this).addClass('pulse');
        }
    });
});
